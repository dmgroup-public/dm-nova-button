<?php

namespace NovaButton\Http\Controllers;

use DMGroup\Managers\CreateSurveyManager;
use Illuminate\Routing\Controller;
use Laravel\Nova\Http\Requests\NovaRequest;

class ButtonController extends Controller
{
    public function handle(NovaRequest $request)
    {
        $event = $request->event;

        $resource = $request->findModelQuery()->firstOrFail();

        $survey = (new CreateSurveyManager('survey', '_self', $resource->id, false))
                                                                            ->init();

        if ($survey) {
            return response()->json([
                'status' => 'true',
                'redirect_url' => $survey
            ]);
        } else {
            return response()->json([
                'status' => 'false'
            ]);
        }
    }
}
